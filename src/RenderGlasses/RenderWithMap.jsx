import React, { Component } from "react";
import "./renderWithMap.css";
import { data } from "./dataGlasses";
export default class RenderWithMap extends Component {
  state = {
    glasses: data,
    glass: {
      name: data[0].name,
      url: data[0].url,
      price: "$" + data[0].price,
      desc: data[0].desc,
    },
  };
  handleChangeGlass = (data) => {
    this.setState({
      glass: {
        name: data.name,
        url: data.url,
        price: "$" + data.price,
        desc: data.desc,
      },
    });
  };

  // this.setState;

  // renderChoseGlass = () => {
  //   return console.log(this.state.glass.name);
  // };
  renderList = () => {
    return this.state.glasses.map((item) => {
      return (
        <>
          <img
            onClick={() => {
              this.handleChangeGlass(item);
            }}
            style={{
              width: `${100 / 3}%`,
              padding: "30px 20px",
            }}
            src={item.url}
            alt=""
          />
        </>
      );
    });
  };
  renderLeft = () => {
    return (
      <div className="col-6 vglasses__left">
        <div className="row">
          <div className="col-12">
            <h1 className="mb-2">Virtual Glasses</h1>
          </div>
        </div>
        <div className="row" id="vglassesList">
          {/* render list kinh  */}
          {this.renderList()}
        </div>
      </div>
    );
  };
  renderRight = () => {
    return (
      <div className="col-5 vglasses__right p-0">
        <div className="vglasses__card">
          <div
            className="vglasses__model"
            style={{
              backgroundImage: `url("./glassesImage/model.jpg")`,
            }}
            id="avatar"
          >
            <img src={this.state.glass.url} alt="" />
          </div>
          <div id="glassesInfo" className="vglasses__info">
            <p>{this.state.glass.name}</p>
            <p>{this.state.glass.price}</p>
            <p>{this.state.glass.desc}</p>
          </div>
        </div>
      </div>
    );
  };
  render() {
    return (
      <div className="container vglasses py-3">
        <div className="row justify-content-between">
          {this.renderLeft()}
          {this.renderRight()}
        </div>
      </div>
    );
  }
}
