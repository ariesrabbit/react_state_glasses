import logo from "./logo.svg";
import "./App.css";
import RenderWithMap from "./RenderGlasses/RenderWithMap";

function App() {
  return (
    <div>
      <RenderWithMap />
    </div>
  );
}

export default App;
